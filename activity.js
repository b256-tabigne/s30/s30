// Fruit on Sale

db.fruits.aggregate([
    {$match:  
        {onSale: true}
    },
    {$count: "fruitOnSale"}
])

// Fruit stock more than 20

db.fruits.aggregate([
    {$match:   
        {stock: {$gte: 20}}
    },
    {$count: "enoughStock"}
])

// Average price per supplier

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id:"$supplier_id", avg_price: {$avg: "$price"}}}
])

// Highest price per supplier
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id:"$supplier_id", max_price: {$max: "$price"}}}
])

// Lowest price per supplier

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id:"$supplier_id", min_price: {$min: "$price"}}}
])